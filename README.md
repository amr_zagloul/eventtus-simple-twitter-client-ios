
Simple Twitter client
Developed on Xcode 8.0 using Swift 3.0

Using Libraries
1 - TwitterKit >> from fabric to handel requests from Twitter APIs 
https://fabric.io/kits/ios/twitterkit

2 - ObjectMapper >> to map the respons of the API to Objects using predefined Models
https://github.com/Hearst-DD/ObjectMapper

3 - Kingfisher  >> to handel image download and cache
https://github.com/onevcat/Kingfisher

4 - ARSLineProgress  >> a simple progress view 
https://github.com/soberman/ARSLineProgress

5 - UILoadControl >> a loading view displayed while loading more data from the server
https://github.com/FelipeCardoso89/UILoadControl