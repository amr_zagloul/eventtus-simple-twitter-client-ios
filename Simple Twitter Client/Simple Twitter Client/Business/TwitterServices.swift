//
//  TwitterServices.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/25/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import Foundation
import TwitterKit
import ObjectMapper

public class TwitterServices {
    
    public static let sharedInstance = TwitterServices()
    var cursor : Int = -1

    func getUserTimeLine(completion:@escaping ([Tweet]?)->Void , id:Int, count:Int , startIndex:Int ) -> Void {
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        let client = TWTRAPIClient(userID: userID )
        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        let params = [ "user_id": "\(id)", "count":"\(count)" , "since_id": "\(startIndex)" ,"trim_user":"1"] as [String : String]
        var clientError : NSError?
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        if  Reachability.isConnectedToNetwork() {
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                print("isConnectedToNetwork = true")
                if connectionError != nil {
                    print("getUserTimeLine >>> Error: \(connectionError)")
                    completion(nil)
                    return
                }
                // parse and mapping data
                self.handelTweetData(completion: completion, data: data!)
            }
        } else {
            completion(nil)
        }
    }
    
    func getUserFollowers(completion:@escaping ([UserFollower]?)->Void , count : Int , page : Int) -> Void {
        if page == 0 {
            cursor = -1
        }
        print("getUserFollowers request page = \(page)  and cursor = \(cursor)")
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        let client = TWTRAPIClient(userID: userID )
        let statusesShowEndpoint = "https://api.twitter.com/1.1/followers/list.json"
        let params = [ "user_id": (Twitter.sharedInstance().sessionStore.session()?.userID)! , "count" : "\(count)" , "cursor" : "\(cursor)" ] as [String : String]
        var clientError : NSError?
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        let cacheResponse = URLCache.shared.cachedResponse(for: request)
        if cacheResponse != nil && !Reachability.isConnectedToNetwork() {
            print("isConnectedToNetwork = false")
            //below two lines will cache the data, request object as key
           let data =  cacheResponse?.data
            self.handelUserData(completion: completion, data: data!)
        } else if  Reachability.isConnectedToNetwork() {
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                print("isConnectedToNetwork = true")
                if connectionError != nil {
                    print("getUserFollowers >>> Error: \(connectionError)")
                    completion(nil)
                    return
                }
                //below two lines will cache the data, request object as key
                let cacheResponse = CachedURLResponse(response: response!, data: data!)
                URLCache.shared.storeCachedResponse(cacheResponse, for: request)
                // parse and mapping data
                self.handelUserData(completion: completion, data: data!)
            }
        } else {
            completion(nil)
        }
    }
    
    func handelTweetData(completion:@escaping ([Tweet])->Void, data: Data )->Void {
            let mapper = Mapper<Tweet>()
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let followersObjects = mapper.mapArray(JSONString: json as! String)
            completion(followersObjects!)
    }
    
    func handelUserData(completion:@escaping ([UserFollower])->Void, data: Data )->Void {
            let mapper = Mapper<UserFollowerResponse>()
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let responseObject = mapper.map(JSONString: json as! String)
            cursor = (responseObject?.next_cursor)!
            completion((responseObject?.users)!)
    }
}
