//
//  CustomUICollectionViewLayout.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 10/5/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit
protocol CustomUICollectionViewLayoutDelegate {
  //  Method to ask the delegate for the height of the cell
  func collectionView(_ collectionView:UICollectionView, heightForItemAtIndexPath indexPath:IndexPath , withWidth:CGFloat) -> CGFloat
}

class CustomUICollectionViewLayoutAttributes:UICollectionViewLayoutAttributes {
  
  var itemHeight: CGFloat = 0.0
  
  // Override copyWithZone to conform to NSCopying protocol
  override func copy(with zone: NSZone?) -> Any {
    let copy = super.copy(with: zone) as! CustomUICollectionViewLayoutAttributes
    copy.itemHeight = itemHeight
    return copy
  }
  
  // Override isEqual
  override func isEqual(_ object: Any?) -> Bool {
    if let attributtes = object as? CustomUICollectionViewLayoutAttributes {
      if( attributtes.itemHeight == itemHeight  ) {
        return super.isEqual(object)
      }
    }
    return false
  }
}


class CustomUICollectionViewLayout: UICollectionViewLayout {
  
  var delegate:CustomUICollectionViewLayoutDelegate!
  
  //Configurable properties
  var numberOfColumns = 1
  var cellPadding: CGFloat = 6.0
  
  //Array to keep a cache of attributes.
  var cache = [CustomUICollectionViewLayoutAttributes]()
  
  // Content height and size
  fileprivate var contentHeight:CGFloat  = 0.0
  fileprivate var contentWidth: CGFloat {
    print("contentWidth")
    let insets = collectionView!.contentInset
    return collectionView!.bounds.width - (insets.left + insets.right)
  }
  
  override class var layoutAttributesClass : AnyClass {
    print("layoutAttributesClass")
    return CustomUICollectionViewLayoutAttributes.self
  }
  
  override func prepare() {
    print("prepareLayout")
    // Only calculate once and one change orientation
    if cache.isEmpty {
    
      // Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
      let columnWidth = contentWidth / CGFloat(numberOfColumns)
      var xOffset = [CGFloat]()
      for column in 0 ..< numberOfColumns {
        xOffset.append(CGFloat(column) * columnWidth )
      }
      var column = 0
      var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
      
      // Iterates through the list of items in the first section
      for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
        
        let indexPath = IndexPath(item: item, section: 0)
        
        //  Asks the delegate for the height of the cell
        let width = columnWidth - cellPadding*2
        let itemHeight = delegate.collectionView(collectionView!, heightForItemAtIndexPath: indexPath , withWidth:width)
        let height = cellPadding +  itemHeight  + cellPadding
        let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
        let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
        
        //  Creates an UICollectionViewLayoutItem with the frame and add it to the cache
        let attributes = CustomUICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.itemHeight = itemHeight
        attributes.frame = insetFrame
        cache.append(attributes)
        
        // Updates the collection view content height
        contentHeight = frame.maxY
        yOffset[column] = yOffset[column] + height
        column = column >= (numberOfColumns - 1) ? 0 : 1+column
      }
    }
  }
  
  
  override var collectionViewContentSize : CGSize {
    print("collectionViewContentSize")
    return CGSize(width: contentWidth, height: contentHeight)
  }
  
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    print("layoutAttributesForElementsInRect")
    var layoutAttributes = [UICollectionViewLayoutAttributes]()
    // Loop through the cache and look for items in the rect
    for attributes  in cache {
      if attributes.frame.intersects(rect ) {
        layoutAttributes.append(attributes)
      }
    }
    return layoutAttributes
  }
}


