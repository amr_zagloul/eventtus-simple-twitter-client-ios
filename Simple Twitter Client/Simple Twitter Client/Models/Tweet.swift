//
//  Tweet.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/28/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import Foundation
import ObjectMapper

public class Tweet : Mappable {
    
    var text: String?
    var created_at: String?
    var id: Int?
    
    required public init?(map: Map){
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        text    <- map["text"]
        created_at <- map["created_at"]
        id    <- map["id"]
    }
}
