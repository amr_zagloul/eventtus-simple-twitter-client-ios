//
//  UserFollower.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/25/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import Foundation
import ObjectMapper

public class UserFollower : Mappable {

    var name: String?
    var image_url: String?
    var id: Int?
    var bio: String?
    var handle: String?
    var backround: String?

    required public init?(map: Map){
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        name    <- map["name"]
        image_url <- map["profile_image_url"]
        id <- map["id"]
        bio <- map["description"]
        handle <- map["screen_name"]
        backround <- map["profile_banner_url"]
    }
}
