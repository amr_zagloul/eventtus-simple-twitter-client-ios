//
//  UserFollowerResponse.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/28/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import Foundation
import ObjectMapper

public class UserFollowerResponse : Mappable {
    
    var users: [UserFollower]?
    var next_cursor : Int = -1
    
    required public init?(map: Map){
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        users    <- map["users"]
        next_cursor <- map["next_cursor"]
    }
}
