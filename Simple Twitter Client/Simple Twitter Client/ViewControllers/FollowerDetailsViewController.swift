//
//  FollowerDetailsViewController.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/28/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit
import ARSLineProgress

class FollowerDetailsViewController: UIViewController {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    
    public var userFollower : UserFollower? = nil
    var tweetsArray : [Tweet]? = nil
    let kCellIdentifier = "TweetCellIdentifier"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if userFollower != nil {
            if self.userFollower?.backround != nil {
                backgroundImageView.kf.setImage(with: URL(string: (self.userFollower?.backround)!))
            } else {
                backgroundImageView.image = UIImage(named: "default")
            }
            self.title = userFollower?.name
            let backItem = UIBarButtonItem(title: NSLocalizedString("back",comment:"Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
//            backItem.title = "Back"
//            self.navigationController?.navigationItem.backBarButtonItem = backItem
            
            self.navigationItem.hidesBackButton = true;
            self.navigationItem.leftBarButtonItem = backItem;
            
            userImageView.kf.setImage(with: URL(string: (self.userFollower?.image_url!)!))
            tableView.dataSource = self
            tableView.register(UINib(nibName: "TweetTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kCellIdentifier)
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 60.0
            tableView.allowsSelection = false
            requestTweets()
        }
    }

    func backButtonPressed() -> Void {
        self.navigationController!.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestTweets() -> Void {
        ARSLineProgress.show()
        TwitterServices.sharedInstance.getUserTimeLine(completion: { ( tweetsArray) in
            ARSLineProgress.hide()
            if (tweetsArray != nil) {
                print(" count of tweetsArray = \(tweetsArray?.count)")
                self.tweetsArray = tweetsArray!;
                self.tableView.reloadData()
            } else {
                let alertController = UIAlertController(title: NSLocalizedString("ERROR", comment: ""), message:
                   NSLocalizedString("NO_INTERNET", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: NSLocalizedString("DISMISS", comment: ""), style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            }, id: (userFollower?.id)!, count: 10, startIndex: 1)
    }
}


/// TableViewDataSource methods.
extension FollowerDetailsViewController: UITableViewDataSource {
    /// Determines the number of rows in the tableView.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetsArray == nil ? 0 : (tweetsArray?.count)! ;
    }
    
    /// Returns the number of sections.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TweetTableViewCell = (tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) as? TweetTableViewCell)!
        // Configure the cell for this indexPath
        let modelItem : Tweet = tweetsArray![indexPath.row]
        cell.tweetTextView.text = modelItem.text
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        return cell
    }
    
}

