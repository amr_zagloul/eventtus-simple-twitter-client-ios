//
//  ViewController.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/23/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit
import TwitterKit
import Crashlytics

class SignInViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let logInButton = TWTRLogInButton { (session, error) in
            if session != nil {
                Crashlytics.sharedInstance().setUserIdentifier(session!.userID)
                Crashlytics.sharedInstance().setUserName(session!.userName)
                self.navigateToMainAppScreen()
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func navigateToMainAppScreen() {
        performSegue(withIdentifier: "ShowUserFollowers", sender: self)
    }

}

