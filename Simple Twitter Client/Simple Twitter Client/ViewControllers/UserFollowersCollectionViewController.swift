//
//  UserFollowersCollectionViewController.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 10/3/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit
import TwitterKit
import Kingfisher
import ARSLineProgress
import UILoadControl


class UserFollowersCollectionViewController: UICollectionViewController {

    var followers :[UserFollower]? = nil
    let kCellIdentifier = "CellIdentifier"
    var refreshControl :UIRefreshControl?
    var currentPage : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib(nibName: "FollowersTableViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: kCellIdentifier)
        self.collectionView?.delegate = self
        loadFollowers(pageIndex: 0)
        self.title = NSLocalizedString("FOLLOWERS", comment: "")
        if let layout = collectionView?.collectionViewLayout as? CustomUICollectionViewLayout {
            layout.delegate = self
        }
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.gray
        refreshControl?.addTarget(self, action: #selector(UserFollowersCollectionViewController.refresh), for: .valueChanged)
        collectionView?.addSubview(refreshControl!)
        collectionView?.alwaysBounceVertical = true
        
        collectionView?.loadControl = UILoadControl(target: self, action: #selector(UserFollowersCollectionViewController.loadMore))
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.loadControl?.update()
    }
    
    func refresh() -> Void {
        self.loadFollowers(pageIndex: 0)
    }
    
    func loadMore() -> Void {
        self.loadFollowers(pageIndex: self.currentPage+1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIDevice.current.orientation.isLandscape{
            (collectionView?.collectionViewLayout as? CustomUICollectionViewLayout)! .numberOfColumns  = 2
        } else {
            (collectionView?.collectionViewLayout as? CustomUICollectionViewLayout)! .numberOfColumns  = 1
        }
    }
    
    func loadFollowers( pageIndex : Int) {
        ARSLineProgress.show()
        TwitterServices.sharedInstance.getUserFollowers(completion: { (follworsArray) in
            ARSLineProgress.hide()
            if (pageIndex == 0) {
                if (self.refreshControl?.isRefreshing)! {
                    self.refreshControl?.endRefreshing()
                }
            } else {
                self.collectionView?.loadControl?.endLoading()
            }
            self.currentPage = pageIndex
            if (follworsArray != nil) {
                print(" count of follworsArray = \(follworsArray?.count)")
                if self.currentPage == 0 {
                    self.followers = follworsArray!
                } else {
                    for user in follworsArray! {
                        self.followers?.append(user)
                    }
                    print(" count of follwors  = \(self.followers?.count)")
                }
                (self.collectionView?.collectionViewLayout as? CustomUICollectionViewLayout)! .cache.removeAll()
                self.collectionView?.reloadData()
            } else {
                let alertController = UIAlertController(title: NSLocalizedString("ERROR", comment: ""), message:
                    NSLocalizedString("NO_INTERNET", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: NSLocalizedString("DISMISS", comment: ""), style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            }, count: 10, page : pageIndex)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        var numberOfCellsPerRow:Int
        if UIDevice.current.orientation.isLandscape{
            numberOfCellsPerRow = 2
            print("landscape")
        } else {
            numberOfCellsPerRow = 1
            print("portrait")
        }
        (collectionView?.collectionViewLayout as? CustomUICollectionViewLayout)! .numberOfColumns = numberOfCellsPerRow
        (collectionView?.collectionViewLayout as? CustomUICollectionViewLayout)! .cache.removeAll()
        collectionView?.reloadData()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return followers == nil ? 0 :  followers!.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FollowersTableViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier, for: indexPath ) as? FollowersTableViewCell)!
        // Configure the cell for this indexPath
        let modelItem : UserFollower = followers![indexPath.row]
        cell.followerNameView.text = modelItem.name
        cell.followerHandelView.text = "@"+modelItem.handle!
        cell.followerImageView.kf.setImage(with: URL(string: modelItem.image_url!))
        if modelItem.bio != nil && modelItem.bio != "" {
            cell.followerBioView.text = modelItem.bio!
        } else  {
            cell.followerBioView.text = ""
        }
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let followerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "FollowerDetailsViewController") as! FollowerDetailsViewController
        
        followerDetailsViewController.userFollower = followers?[indexPath.row]
        self.navigationController?.pushViewController(followerDetailsViewController, animated: true)
    }

}

extension UserFollowersCollectionViewController : CustomUICollectionViewLayoutDelegate {

    func collectionView(_ collectionView:UICollectionView, heightForItemAtIndexPath indexPath:IndexPath , withWidth width:CGFloat) -> CGFloat {
        let user = followers?[(indexPath as NSIndexPath).item]
        let description = user?.bio
        let margin: CGFloat = 20.0
        var height: CGFloat = 100 ; // hard code the height of items on the cell with out the description view
        if description != nil && description != "" {
            let context: NSStringDrawingContext = NSStringDrawingContext()
            context.minimumScaleFactor = 0.8
            let size: CGSize = description!.boundingRect(with: CGSize(width:width - margin,  height : CGFloat(MAXFLOAT)), options:NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16)], context: context).size as CGSize
            height = height + size.height
            print(" description = \(description) ")
        }
        return height
    }
}
