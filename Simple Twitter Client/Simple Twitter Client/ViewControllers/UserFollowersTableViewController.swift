////
////  UserFollowersTableViewController.swift
////  Simple Twitter Client
////
////  Created by amr zagloul on 9/24/16.
////  Copyright © 2016 SolGen. All rights reserved.
////
//
//import UIKit
//import TwitterKit
//import Kingfisher
//import ARSLineProgress
//
//class UserFollowersTableViewController: UITableViewController {
//
//    var followers :[UserFollower]? = nil
//    let kCellIdentifier = "CellIdentifier"
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableView.register(UINib(nibName: "FollowersTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kCellIdentifier)
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 120.0
//        loadFollowers()
//        self.title = "Followers"
//    }
//    
//    func loadFollowers() {
//        ARSLineProgress.show()
//        TwitterServices.sharedInstance.getUserFollowers { (follworsArray) in
//            ARSLineProgress.hide()
//            if (follworsArray != nil) {
//                print(" count of follworsArray = \(follworsArray?.count)")
//                self.followers = follworsArray!;
//                self.tableView.reloadData()
//            } else {
//                let alertController = UIAlertController(title: "Error", message:
//                    "No Internet connection available", preferredStyle: UIAlertControllerStyle.alert)
//                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
//                self.present(alertController, animated: true, completion: nil)
//            }
//        }
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return followers == nil ? 0 :  followers!.count
//    }
//
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: FollowersTableViewCell = (tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) as? FollowersTableViewCell)!
//        // Configure the cell for this indexPath
//        let modelItem : UserFollower = followers![indexPath.row]
//        cell.followerNameView.text = modelItem.name
//        cell.followerHandelView.text = "@"+modelItem.handle!
//        cell.followerImageView.kf.setImage(with: URL(string: modelItem.image_url!))
//        if modelItem.bio != nil && modelItem.bio != "" {
//            cell.followerBioView.text = modelItem.bio!
//        } else  {
//            cell.followerBioView.text = ""
//        }
//        cell.setNeedsUpdateConstraints()
//        cell.updateConstraintsIfNeeded()
//        return cell
//    }
// 
//    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let followerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "FollowerDetailsViewController") as! FollowerDetailsViewController
//        
//        followerDetailsViewController.userFollower = followers?[indexPath.row]
//        self.navigationController?.pushViewController(followerDetailsViewController, animated: true)
//    }
//}
