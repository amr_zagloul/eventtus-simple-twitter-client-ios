//
//  FollowersTableViewCell.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/25/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit

class FollowersTableViewCell: UICollectionViewCell {

    
    @IBOutlet weak var followerImageView: UIImageView!
    @IBOutlet weak var followerNameView: UILabel!
    @IBOutlet weak var followerHandelView: UILabel!
    @IBOutlet weak var followerBioView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}
