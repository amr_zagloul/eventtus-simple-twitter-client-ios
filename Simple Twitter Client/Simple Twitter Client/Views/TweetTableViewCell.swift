//
//  TweetTableViewCell.swift
//  Simple Twitter Client
//
//  Created by amr zagloul on 9/29/16.
//  Copyright © 2016 SolGen. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    @IBOutlet var tweetTextView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
